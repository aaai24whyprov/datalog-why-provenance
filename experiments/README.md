## SCENARIOS
This directory is structured as follows:
- The "bin" directory contains binaries needed by the python scripts inside this directory. In particular, DLV2 and why-sat.
- The "datasets" directory contains the experimental scenarios used in our paper. In particular, each directory inside "datasets" corresponds to a scenario, and it is structured as follows:
    - "program.lp" is the file containing the Datalog query of the scenario in ASP syntax.
    - "databases" is the directory containing the databases of the scenario.
    - "queries" contains one file for each database of the scenario, and each such file contains the 100 randomly selected tuples for this scenario's query w.r.t. the corresponding database.
- "downward" is an empty directory where the downward closures will be stored.
- "times" is an empty directory where logs about running time will be stored.
- "kr_vlog" contains the Doctors scenarios in the format supported by the on demand why-provenance approach of the paper [1].
- "build-downward-closure.py" is the code responsible for computing the downward closure. Please refer to the README in the root of this repository on how to use it.
- "build-why-provenance.py" is the code responsible for running the why-sat tool for constructing the why-provenance. Please refer to the README in the root of this repository on how to use it.
- "csv_to_facts.py" is a convenience script that converts each row in a comma-separated CSV file to a fact over a specified predicate name.
 
**Remark:** It is important to not change the internal structure of the "datasets" directory, as this is what our Python scripts expect when running the experiments.

[1] Ali Elhalawati, Markus Krotzsch, Stephan Mennicke. An Existential Rule Framework for Computing Why-Provenance On-Demand for Datalog
