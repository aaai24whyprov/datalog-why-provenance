OVERVIEW
========
This repository contains the source code and the experimental scenarios that have been used for the experimental evaluation of the paper:

"Anonymous Authors. Computing the Why-Provenance for Datalog Queries via SAT Solvers"

The repository is structured in the following directories:

* experiments: This directory contains the experimental scenarios employed in the experimental evaluation of the paper, together with the Python code implementing the construction of the downward closure, needed by the why-sat tool. Further details of the internal structure of this directory can be found in the README inside the directory itself.
* why-sat: contains the C++ source code that enumerates all the members of the why-provenance of a given tuple via the Glucose SAT solver. In particular, given as input the downward closure of a fact w.r.t. some database and Datalog query, it will convert it into a CNF Boolean formula, according to the reduction discussed in the paper, and then enumerate the why-provenance elements using Glucose. Further details on how to use it will be given later in this README, while details about how to compile the code are given in the dedicated README inside the why-sat directory.


All the code of this repository has been designed for a Linux environment, and has been tested on both Fedora and Ubuntu.

CLONING
=======
This repository has submodules, so it is suggested to clone like:  

`git clone --recurse-submodules --depth 1 https://gitlab.com/aaai24whyprov/datalog-why-provenance.git`

HOW TO USE
========
Before starting, due to space limitations in GitLab, all our scenarios have been compressed in a .tar.gz file. So, we first need to decompress it in the right directory. Open a terminal in the "experiments" directory and type:

`tar -xvzf datasets/extract_here.tar.gz -C datasets`

All the scenarios should now be found inside "experiments/datasets", and you can safely delete the .tar.gz once all files have been extracted.


The following describes a full workflow from constructing the downward closure of a fact, to running the why-sat tool to enumerate members of the why-provenance.
All our Python code requires Python 3. To install it:
* `sudo apt install python3` (Debian and Ubuntu based)
* `sudo dnf install python3` (Fedora)

Please verify that `python --version` shows version 3. **You might need to type `python3`, and not just `python`, in the following commands.**

## Constructing the downward closure
Open a terminal in the "experiments" directory. We will use the script "build-downward-closure.py" to build the downward closure for the facts of all scenarios in the "datasets" directory (for further details on how they are structured please refer to the paper and the README in the "experiments" directory). For example, to construct the downward closure for the facts of the 
Galen scenario, type:

`python build-downward-closure.py galen .`

where "galen" is the name of the actual directory inside the "datasets" directory containing the Galen scenario, while "." denotes the directory where to store the downward closures. In this case, it will use the current directory as a reference, and will place the downward closures inside ./downward.

**Note**: The above script relies on the Linux x64 DLV2 binary inside the bin directory. So, before running the above script, you might need to make the binary executable with:

`sudo chmod +x bin/dlv2`

## Constructing the Why-Provenance (why-sat)
With the downward closures in place, we can now use the why-sat tool to convert them into CNF formulas and enumerate members of the why-provenance. For this, we first need to compile why-sat (please refer to the instructions in the README inside the "why-sat" directory). Once compiled, we need to move the obtained `why-sat` binary inside the "experiments/bin" directory, as it is required by the Python scripts.

For convenience, we already ship a prebuilt Linux x64 `why-sat` binary inside the "experiments/bin" directory. As did for DLV, be sure to make it executable.

Open now a terminal inside the "experiments" directory. To construct the why-provenance for the Galen scenario, type:

`python build-why-provenance.py galen . <timeout> <num-supports>`

where "galen" is the directory inside "datasets" containing the Galen scenario. The directory "." is where logs including running times should be stored; in this case, in "./times". The parameter "timeout" takes any value that the `timeout` unix command accepts, e.g. 5s for 5 seconds, 10m for 10 minutes. If 0 is given, no timeout will be set. The parameter "num-supports" specifies how many members of the why-provenance to construct at most. If 0 is given, all of them will be constructed.

COMPARING WITH OTHER APPROACHES
========
In our paper, we compare with an approach based on existential rules for computing the why-provenance w.r.t. standard proof trees, introduced in [1]. The scenarios over which we make the comparison are stored in "experiments/kr_vlog". These are the same Galen and Doctors-based scenarios we consider, but in a format suitable for the approach of [1]. Here we explain how to run the technique of [1] over these scenarios.

A modern version of Java is required for this. Our tests have been run with OpenJDK 17.

Enter the "experiments/kr_vlog/" directory, open a terminal inside this directory, and execute the script:

`./run_krvlog.sh > time_logs.tsv`

You might need to make the script executable first, using 

`sudo chmod +x run_krvlog.sh`

**Note:**
The above script uses the latest version of the rulewerk binary as of the time of writing, which is the java interface for VLog.

After completion, the overall running times for building the why-provenance of all scenarios should be found in the time_logs.tsv file, which is a csv-like file with tabs as the separators, attaching the total running time to each tuple of each scenario. All times are in milliseconds, and the script sets a default timeout of 5 minutes.

To compute only the downward closure using the approach of [1], execute the script:

`./run_krvlog_downward_only.sh > downward_logs.tsv`

The above scripts can be terminated earlier using Ctrl+Z on the terminal.

[1] Ali Elhalawati, Markus Krotzsch, Stephan Mennicke. An Existential Rule Framework for Computing Why-Provenance On-Demand for Datalog

