#include "glucosecnfbuilder.h"

dexp::glucosecnfbuilder::glucosecnfbuilder() :
    m_builtin(true) {
#ifdef INCREMENTAL
    m_current_solver.setIncrementalMode();
#endif
    m_current_solver.verbosity = 0;
    m_current_solver.verbEveryConflicts = 0;
}


dexp::cnfbuilder& dexp::glucosecnfbuilder::set_cnf_info(size_t num_vars, size_t num_clauses, const std::vector<atom_graph>& components) {

    while (m_current_solver.nVars() < num_vars)
		m_current_solver.newVar();

    if(m_builtin){

        /*
        size_t num_vertices = 0;
        for(auto& comp : components){
            if(comp.size() > 1){
                num_vertices += comp.size();
            }
        }

	    m_satgraph.createGraph(num_vars);
        m_satgraph.initGraph(num_vertices);

        std::map<int,int> node_to_index;
        int node_index = 0;

        for(auto& comp : components){
            if(comp.size() > 1){
                for(auto& node_id : comp){
                    auto out_degree = comp.count_out_neighbors(node_id);
                    m_satgraph.createNode(node_index, out_degree);
                    node_to_index[node_id]=node_index;
                    node_index++;
                }

                for(auto& source : comp){
                    auto edge_it = comp.out_neighbors(source);
                    for(auto it = edge_it.first; it != edge_it.second; ++it){
                        auto target = it->first;
                        auto edge_id = it->second.prop();
                        auto edge_literal = mkLit(edge_id);

                        m_satgraph.addArc(edge_literal,node_to_index[source],node_to_index[target]);
                    }
                }
            }
        }
        

        m_satgraph.createInverseArcs();

        m_propagator = std::make_unique<AcyclicityPropagator>(&m_current_solver, &m_satgraph);
        m_current_solver.addPropagator(m_propagator.get());*/
    }
    
    return *this;
}

dexp::cnfbuilder& dexp::glucosecnfbuilder::begin_clause() {
    if(m_working_on_clause){
        throw std::logic_error("Another clause is already open, please close it first!");
    }

    m_working_on_clause = true;
    m_num_literals_current_clause = 0;
    m_lits.clear();

    return *this;
}

dexp::cnfbuilder& dexp::glucosecnfbuilder::end_clause() {
    if(!m_working_on_clause){
        throw std::logic_error("No clause has been opened. Cannot close");
    }

    if(m_num_literals_current_clause == 0){
        throw std::logic_error("Trying to add an empty clause!");
    }

	m_current_solver.addClause_(m_lits);
	//m_mxsolver->addHardClause(m_lits);
    
    m_working_on_clause = false;

    return *this;
}

dexp::cnfbuilder& dexp::glucosecnfbuilder::add_pos_literal(int var_id) {
    int var = var_id;
    while (var >= m_current_solver.nVars()) m_current_solver.newVar();
    m_lits.push(Glucose::mkLit(var));
    m_num_literals_current_clause++;
    return *this;
}

dexp::cnfbuilder& dexp::glucosecnfbuilder::add_neg_literal(int var_id) {
    int var = var_id;
    while (var >= m_current_solver.nVars()) m_current_solver.newVar();
    m_lits.push(~Glucose::mkLit(var));
    m_num_literals_current_clause++;
    return *this;
}

Glucose::Solver& dexp::glucosecnfbuilder::solver(){
    return m_current_solver;
}