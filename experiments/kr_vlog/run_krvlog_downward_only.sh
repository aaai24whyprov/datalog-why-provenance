#!/bin/sh

# Running doctors
for benchmark in doctors galen; do
    cd $benchmark
    rm -rf temp.txt || true
    for program in downward_only/*.txt; do
        program_basename="$(basename $program)"
        program_basename="${program_basename%.*}"
        for db in databases/*.txt; do
            db_basename="$(basename $db)"
            db_basename="${db_basename%.*}"
            tuples_file="queries/$db_basename/$program_basename.txt"
            while read tuple; do
                cat $db $program <(echo $tuple) > temp.txt
                result_text=$(echo -e "@load 'temp.txt' .\n @reason" | timeout -s SIGTERM 5m java -jar ../bin/rulewerk-client-0.9.0.jar 2>&1)
                elapsed_time=$(echo $result_text | grep ".*finished in.*" | sed -E 's/.*finished in ([0-9]+)ms.*/\1/')
                crashed=$(echo $result_text | grep "ERROR")
                echo -en "$program_basename\t$db_basename\t\"$tuple\"\t"
                if [ -z "$crashed" ]
                then
                    if [ -z "$elapsed_time" ]
                    then
                        echo "-1"
                    else
                        echo $elapsed_time
                    fi
                else
                    echo "-2"
                fi
                rm -rf temp.txt || true
            done < $tuples_file
        done
    done
    cd ..
done
